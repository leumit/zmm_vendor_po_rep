sap.ui.define(["sap/ui/model/json/JSONModel","sap/m/MessageBox", "sap/ui/Device","sap/ui/model/Filter"], function (JSONModel,MessageBox, Device,Filter) {
    "use strict";

    return {
        createDeviceModel: function () {
            var oModel = new JSONModel(Device);
            oModel.setDefaultBindingMode("OneWay");
            return oModel;
        },
		loadReportData: function () {
			debugger;
			var oModel = oComponent_venPoRep.getModel("ODATA");
			var oModelData = oComponent_venPoRep.getModel("JSON").getData();
			const sKey = oModel.createKey("/OrderRepOrderStatusSet", {
				IvBeginDate: this.getFixedDate(oModelData.BeginDate),
				IvEkgrp: '',
				IvEndDate: this.getFixedDate(oModelData.EndDate),
				IvOrderStatus: ''
			});
			return new Promise((resolve, reject) => {
				oModel.read(sKey, {
					urlParameters: {
						"$expand": "OrderReport,OrderRepPurGrpGrahp,OrderRepVendorGrahp"
					},
					success: (data) => {
						resolve(data);
					},
					error: (error) => {
						reject(error);
					}
				});
			});
		},
		LoadOutput: function(sOrderNum){
			var sKey = oComponent_venPoRep.getModel("ODATA").createKey("/OrderPdfOutputSet", {
				IvPoNumber : sOrderNum
			})
            return new Promise(function(resolve, reject) {
				oComponent_venPoRep.getModel("ODATA").read(sKey, {
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
		handleErrors: function (error) {
			try {
				MessageBox.error(JSON.parse(error.responseText).error.message.value);
			} catch (e) {
				MessageBox.error(JSON.stringify(error.message));
			}
		},
        
        createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
                Filters: [],
				titleClickable: true,
				BeginDate: new Date(new Date().setDate(new Date().getDate() - 90)),
				EndDate: new Date(),
				lastDate: new Date(),
				GraphType1: 'Quantity',
				vandorTxt: 'ספק',
				DialogGraphType1: 'Quantity',
				DialogGraphType3: 'Quantity',
				GraphType3: 'Quantity',
				closeHeader: false,
				headerExpanded: true

			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		getFixedDate: function (value) {
			if (!value) {
				return;
			}
			var before = value.getTime();
			var after = new Date(before + 10800000); //adding three hours to fix UTC gateway processing day back
			return after;
		}
    };
});
