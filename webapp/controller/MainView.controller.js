/* global oComponent_venPoRep  : true */
sap.ui.define(["ZMM_VENDOR_PO_REP/controller/BaseController",'sap/ui/export/library', "sap/ui/model/FilterOperator", "sap/ui/model/Filter", "sap/ui/export/Spreadsheet", "ZMM_VENDOR_PO_REP/model/models", "ZMM_VENDOR_PO_REP/model/formatter"], function (Controller,exportLibrary, FilterOperator,Filter, Spreadsheet, models, formatter) {
	"use strict";

	var EdmType = exportLibrary.EdmType;

	return Controller.extend("ZMM_VENDOR_PO_REP.controller.MainView", {
		/**
		 * @override
		 */
		formatter: formatter,
		onInit: function (oEvent) {
			oComponent_venPoRep._MainController = this;
			this.getData();


		},

		getData: function () {
			debugger;
			oComponent_venPoRep.getModel("ODATA").metadataLoaded().then(() => {
				models.loadReportData().then(function (data) {
					var aVendor = [],
						aPurgroup = [],
						aStatus = [];
					for (var i = 0; i < data.OrderReport.results.length; i++) {
						aVendor.push({
							"Vendor": data.OrderReport.results[i].Vendor,
							"Vendorname": data.OrderReport.results[i].Vendorname
						});
						aPurgroup.push({
							"Purgroup": data.OrderReport.results[i].Purgroup,
							"Purgroupname": data.OrderReport.results[i].Purgroupname
						});
						aStatus.push({
							"Orderstatus": data.OrderReport.results[i].Orderstatus,
							"OrderstatusTxt": data.OrderReport.results[i].OrderstatusTxt
						});
					}
					aVendor = oComponent_venPoRep._MainController.removeDuplicates(aVendor, "Vendor");
					aPurgroup = oComponent_venPoRep._MainController.removeDuplicates(aPurgroup, "Purgroup");
					aStatus = oComponent_venPoRep._MainController.removeDuplicates(aStatus, "Orderstatus");
					oComponent_venPoRep.getModel("JSON").setProperty("/VendorArray", aVendor);
					oComponent_venPoRep.getModel("JSON").setProperty("/PurgroupArray", aPurgroup);
					oComponent_venPoRep.getModel("JSON").setProperty("/StatusArray", aStatus);
					oComponent_venPoRep.getModel("JSON").setProperty("/orderData", data);
					oComponent_venPoRep.getModel("JSON").setProperty("/count", data.OrderReport.results.length);
					oComponent_venPoRep._MainController.filterItems();
					oComponent_venPoRep._MainController.getChartData(data);
				}).catch((error) => {
					models.handleErrors(error);
				});
			});
		},
		headerBtnClicked: function (flag) {
			oComponent_venPoRep.getModel("JSON").setProperty("/closeHeader", !flag);
		},
		getChartData: function (data) {
			var aStatus = [];
			for (var i = 0; i < data.OrderRepVendorGrahp.results.length; i++) {
				data.OrderRepVendorGrahp.results[i].Amount = parseFloat(data.OrderRepVendorGrahp.results[i].Amount);
			}
			for (var i = 0; i < data.OrderRepPurGrpGrahp.results.length; i++) {
				data.OrderRepPurGrpGrahp.results[i].Amount = parseFloat(data.OrderRepPurGrpGrahp.results[i].Amount);
			}
			var status = [
				{ text: "התקבלה במלואה", count: data.Complited, color:"#107e3e" },
				{ text: "התקבלה חלקית", count: data.PartlyReceipted, color:"#f59a23" },	
				{ text: "נוצרה וטרם אושרה", count: data.InApproval, color: "#d9001b"},
				{ text: "פתוחה", count: data.Sent , color:"#0854a1"},
				{ text: "טיוטה", count: data.Draft ,color:"#8a8f91"}

			];
			oComponent_venPoRep.getModel("JSON").setProperty("/status", status);
			this.buildGraph();
		},
		createFrameDialog: function (graphDialog , sGraph) {
			debugger;
			var graph = oComponent_venPoRep._MainController.getView().byId(sGraph);
			graphDialog.setVizProperties(graph.getVizProperties());
			graphDialog.setDataset(graph.getDataset());
			var sPath = sGraph === 'sidVizFrame1' ? 'OrderRepVendorGrahp' : 'OrderRepPurGrpGrahp';
			var sType = sGraph === 'sidVizFrame1' ? oComponent_venPoRep.getModel("JSON").getProperty("/DialogGraphType1") : oComponent_venPoRep.getModel("JSON").getProperty("/DialogGraphType3");
			this.onChangeDisplay('',sGraph+'Dialog',sPath,sType);
		},

		filterItems: function (oEvent) {
			debugger;
			var aFilters = [];
			var model = oComponent_venPoRep.getModel("JSON");
			var fData = model.getProperty("/Filters");
			if (fData.PurgroupSh && fData.PurgroupSh.length) {
				for (const element of fData.PurgroupSh) {
					aFilters.push(new sap.ui.model.Filter("Purgroup", "EQ", element.Purgroup));
				}
			}

			if (fData.VendorSh && fData.VendorSh.length) {
				for (const element of fData.VendorSh) {
					aFilters.push(new sap.ui.model.Filter("Vendor", "EQ", element.Vendor));
				}
			}
			if (fData.Status && fData.Status.length) {
				for (const element of fData.Status) {
					aFilters.push(new sap.ui.model.Filter("Orderstatus", "EQ", element));
				}
			}
			if (fData.OrderNum) {
					aFilters.push(new sap.ui.model.Filter("Ordernumber", FilterOperator.Contains , fData.OrderNum));
			}

			var oTable = oComponent_venPoRep._MainController.getView().byId("dataTable");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilters, "Application");
			var count = oTable.getBinding("items").getLength();
			oComponent_venPoRep.getModel("JSON").setProperty("/count", count);

		},
		clearFilters: function (oEvent) {
			var model = oComponent_venPoRep.getModel("JSON"),
				modelData = model.getData();
			model.setProperty("/Filters", []);
			this.filterItems();
		},
		getOutput: function (םEvent, sOrderNum) {

		},
		createColumnConfig: function() {
			var aCols = [];

			aCols.push({
				label: 'מס. הזמנה',
				property: 'Ordernumber'
			});

			aCols.push({
				label: 'ספק',
				property: 'Vendorname'
			});

			aCols.push({
				label: 'תאריך מסמך',
				property: 'Createdat',
				type: EdmType.Date
			});

			aCols.push({
				label: 'כמות פריטים',
				property: 'Itemcount'
			});

			aCols.push({
				label: 'קבוצה',
				property: 'Purgroupname'
			});

			aCols.push({
				label: 'ערך הזמנה',
				property: ['Ordervalue', 'Currencycode'],
				template: '{0}, {1}'
			});

			aCols.push({
				label: 'סטטוס הזמנה',
				property: 'OrderstatusTxt'
			});

			return aCols;
		},
		// createColumnConfig: function () {
		// 	var aCols = oComponent_venPoRep._MainController.byId("dataTable").getColumns();
		// 	var aColumns = [];
		// 	for (var i = 0; i < aCols.length; i++) {
		// 		if (oComponent_venPoRep._MainController.byId("dataTable").getColumns()[i].getVisible()) {
		// 			aColumns.push({
		// 				label: aCols[i].getTooltip() || " ",
		// 				property: aCols[i].getFilterProperty() || " ",
		// 				width: '15rem'
		// 			});
		// 		}
		// 	}
		// 	return aColumns;

		// },
		onExport: function () {
			var aCols, aItems, oSettings, oSheet;
			aCols = this.createColumnConfig();
			aItems = oComponent_venPoRep.getModel("JSON");
			var aIndices = oComponent_venPoRep._MainController.byId("dataTable").getBinding("items").aIndices;
			var aSelectedModel = [];
			for (var i = 0; i < aIndices.length; i++) {
				aSelectedModel.push(aItems.getProperty("/orderData/OrderReport/results/" + aIndices[i]));
			}
			// var aSelectedModel = oComponent_venPoRep._MainController.byId("dataTable").getBinding("rows").oList;
			oSettings = {
				workbook: {
					columns: aCols
				},
				dataSource: aSelectedModel,
				fileName: "בקרת הזמנות רכש"
			};

			oSheet = new Spreadsheet(oSettings);
			oSheet.build()
				.then(function () { });

		},
		removeDuplicates: function (array, key) {
			let lookup = new Set();
			return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
		}
	});
});
