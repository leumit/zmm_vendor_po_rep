/* global oComponent_venPoRep  : true */
sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/routing/History",
        "sap/ui/core/UIComponent",
        "sap/ui/model/Filter",
        "sap/ui/model/Sorter",
        "sap/ui/model/FilterOperator",
        "ZMM_VENDOR_PO_REP/model/formatter",
        "ZMM_VENDOR_PO_REP/model/models",
        "sap/m/MessageBox",
        'sap/viz/ui5/format/ChartFormatter',
        'sap/viz/ui5/api/env/Format'
    ],
    function (Controller, History, UIComponent, Filter, Sorter, FilterOperator, formatter, models, MessageBox, ChartFormatter, Format) {
        "use strict";

        return Controller.extend("ZMM_VENDOR_PO_REP.controller.BaseController", {
            formatter: formatter,

            /**
             * Convenience method for getting the view model by name in every controller of the application.
             * @public
             * @param {string} sName the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            onInit: function (oEvent) {
                debugger;

            },
            getModel: function (sName) {
                return this.getView().getModel(sName);
            },

            /**
             * Convenience method for setting the view model in every controller of the application.
             * @public
             * @param {sap.ui.model.Model} oModel the model instance
             * @param {string} sName the model name
             * @returns {sap.ui.mvc.View} the view instance
             */
            setModel: function (oModel, sName) {
                return this.getView().setModel(oModel, sName);
            },

            /**
             * Convenience method for getting the resource bundle.
             * @public
             * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
             */
            getResourceBundle: function () {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle();
            },

            /**
             * Method for navigation to specific view
             * @public
             * @param {string} psTarget Parameter containing the string for the target navigation
             * @param {Object.<string, string>} pmParameters? Parameters for navigation
             * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
             */
            navTo: function (psTarget, pmParameters, pbReplace) {
                this.getRouter().navTo(psTarget, pmParameters, pbReplace);
            },

            getRouter: function () {
                return UIComponent.getRouterFor(this);
            },
            onOpenVizFrameDialog: function (oEvent, sDialogName, sGraph) {
                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment('vizFrameDialog', "ZMM_VENDOR_PO_REP.view.fragments." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                var _graphDialog = sap.ui.core.Fragment.byId("vizFrameDialog", sGraph + "Dialog");
                oComponent_venPoRep.getModel("JSON").setProperty("/GraphDialogType", sGraph);
                oComponent_venPoRep._MainController.createFrameDialog(_graphDialog, sGraph);
                this[sDialogName].open();
            },
            onOpenDialog: function (oEvent, sDialogName) {
                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("ZMM_VENDOR_PO_REP.view.fragments." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();
            },
            onCloseDialog: function (sDialogName) {
                this[sDialogName].close();
            },
            onValueHelpSearch: function (oEvent, sType) {
                var sValue = oEvent.getParameter("value");
                var oFilter = [];
                if (sType === 'Vendor') {
                    oFilter = new Filter({
                        filters: [new Filter("Vendor", FilterOperator.Contains, sValue),
                        new Filter("Vendorname", FilterOperator.Contains, sValue)
                        ],
                        and: false
                    });

                }
                else if (sType === 'Purgroup') {
                    oFilter = new Filter({
                        filters: [new Filter("Purgroup", FilterOperator.Contains, sValue),
                        new Filter("Purgroupname", FilterOperator.Contains, sValue)
                        ],
                        and: false
                    });
                }

                var oBinding = oEvent.getParameter("itemsBinding");
                oBinding.filter(oFilter);
            },
            onDeleteToken: function (oEvent, sType) {
                debugger;
                const sKey = oEvent.getParameter("token").getBindingContext("JSON").getObject()[sType];
                const model = oComponent_venPoRep.getModel("JSON");
                var aTokens = model.getProperty("/Filters/" + sType + 'Sh');
                const index = this.findIndexToDelete(aTokens, sKey);
                aTokens.splice(index, 1);
                model.setProperty("/Filters/" + sType + 'Sh', aTokens);
                this.filterItems();
            },
            onDeleteGrahpToken: function (oEvent, sType, VizGraph2) {
                debugger;
                const sKey = oEvent.getParameter("token").getBindingContext("JSON").getObject()[sType];
                var sGraphType1 = oComponent_venPoRep.getModel("JSON").getProperty("/GraphType1");
                var sGraphType3 = oComponent_venPoRep.getModel("JSON").getProperty("/GraphType3");
                const model = oComponent_venPoRep.getModel("JSON");
                if (sType === 'Purgroup') {
                    var aTokens = model.getProperty("/PurgroupGrahp");
                }
                else if (sType === 'Vendor') {
                    var aTokens = model.getProperty("/vendorGrahp");
                }
                const index = this.findIndexToDelete(aTokens, sKey);
                aTokens.splice(index, 1);
                if (sType === 'Purgroup') {
                    model.setProperty("/PurgroupGrahp", aTokens);
                    this.onFilterGraph('', 'idVizFrame3', sGraphType3, VizGraph2);
                }
                else if (sType === 'Vendor') {
                    model.setProperty("/vendorGrahp", aTokens);
                    this.onFilterGraph('', 'idVizFrame1', sGraphType1, VizGraph2);
                }

                // this._filter();
            },
            findIndexToDelete: function (aTokens, sKey) {
                for (var i = 0; i < aTokens.length; i++) {
                    if (sKey === aTokens[i].key) {
                        return i;
                    }
                }
            },
            onValueHelpClose: function (oEvent, sType) {
                debugger;
                var oSelectedItem = oEvent.getParameter("selectedItems");
                var sGraphType1 = oComponent_venPoRep.getModel("JSON").getProperty("/GraphType1");
                var sGraphType3 = oComponent_venPoRep.getModel("JSON").getProperty("/GraphType3");
                var aItems = [];
                oEvent.getSource().getBinding("items").filter([]);

                if (!oSelectedItem) {
                    return;
                }
                else if (sType === 'Vendor') {
                    for (var i = 0; i < oSelectedItem.length; i++) {
                        aItems.push({ Vendor: oSelectedItem[i].getProperty("description"), Vendorname: oSelectedItem[i].getProperty("title") });
                    }
                    oComponent_venPoRep.getModel("JSON").setProperty("/Filters/VendorSh", aItems);
                    this.filterItems();
                }
                else if (sType === 'Purgroup') {
                    for (var i = 0; i < oSelectedItem.length; i++) {
                        aItems.push({ Purgroup: oSelectedItem[i].getProperty("description"), Purgroupname: oSelectedItem[i].getProperty("title") });
                    }
                    oComponent_venPoRep.getModel("JSON").setProperty("/Filters/PurgroupSh", aItems);
                    this.filterItems();
                }
                else if (sType === 'PurgroupGrahp') {
                    for (var i = 0; i < oSelectedItem.length; i++) {
                        aItems.push({ Purgroup: oSelectedItem[i].getProperty("description"), Purgroupname: oSelectedItem[i].getProperty("title") });
                    }
                    oComponent_venPoRep.getModel("JSON").setProperty("/PurgroupGrahp", aItems);
                    this.onFilterGraph('', 'idVizFrame3', sGraphType3);
                }
                else if (sType === 'VendorGrahp') {
                    for (var i = 0; i < oSelectedItem.length; i++) {
                        aItems.push({ Vendor: oSelectedItem[i].getProperty("description"), Vendorname: oSelectedItem[i].getProperty("title") });
                    }
                    oComponent_venPoRep.getModel("JSON").setProperty("/vendorGrahp", aItems);
                    this.onFilterGraph('', 'idVizFrame1', sGraphType1, 'idVizFrame1Dialog');
                }
            },
            onNavBack: function () {
                var sPreviousHash = History.getInstance().getPreviousHash();

                if (sPreviousHash !== undefined) {
                    window.history.back();
                } else {
                    this.getRouter().navTo("appHome", {}, true /*no history*/);
                }
            },
            openOutput: function (sOrderNum) {
                models.LoadOutput(sOrderNum).then(function (data) {
                    var byteCharacters = atob(data.EvOutput);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    if (window.navigator.msSaveOrOpenBlob) {
                        var blob = new Blob([byteArray], { type: 'application/pdf' });
                        window.navigator.msSaveOrOpenBlob(blob, "Draft" + ".pdf");
                    }
                    else {
                        var file = new Blob([byteArray], { type: 'application/pdf;base64' });
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                    }
                }).catch(function (error) {
                    try {
                        MessageBox.error(JSON.parse(error.responseText).error.message.value);
                    } catch (e) {
                        MessageBox.error(JSON.stringify(error));
                    }
                });

            },
            goToApp: function (oEvent, orderNum) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "zmm_create_vendor_po-create";
                var actionURL = "create&/master/" + orderNum;
                // var oParams = {
                //     OrderNumber: orderNum
                // };
                // this.setAppState(oCrossAppNavigator);
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: 'zmm_create_vendor_po',
                                    action: actionURL
                                }
                            })) || "";
                            sap.m.URLHelper.redirect(hash, true);
                        }

                    })
                    .fail(function () { });
            },
            GoToMMPURPAMEPO: function (event, sOrder) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "MMPURPAMEPO-display";
                var oParams = {
                    PurchaseOrder: sOrder
                };
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: "MMPURPAMEPO",
                                    action: "display"
                                },
                                params: oParams
                            })) || "";
                            oCrossAppNavigator.toExternal({
                                target: {
                                    shellHash: hash
                                }
                            });
                        }

                    })
                    .fail(function () { });
            },
            buildGraph: function () {
                debugger;
                Format.numericFormatter(ChartFormatter.getInstance());
                var formatPattern = ChartFormatter.DefaultPattern;
                var pieItems = oComponent_venPoRep.getModel("JSON").getProperty("/status");
                var aPieColors = [];
                for (var i = 0; i < pieItems.length; i++) {
                    if (pieItems[i].count !== 0) {
                        aPieColors.push(pieItems[i].color);
                    }

                }
                var _graph1 = this.byId("idVizFrame1");
                var _graph2 = this.byId("idVizFrame2");
                var _graph3 = this.byId("idVizFrame3");
                var oPopOver = this.getView().byId("idPopOver");
                var oProperties1 = {
                    plotArea: {
                        formatString: formatPattern.SHORTFLOAT_MFD2,
                        dataLabel: {
                            visible: true,
                            showTotal: true,
                            hideWhenOverlap: false,
                        }

                    },
                    legend: {
                        visible: false
                    },

                    title: {
                        visible: false
                    },
                    valueAxis: {
                        label: {
                            formatString: formatPattern.SHORTFLOAT
                        },
                        title: {
                            text: window.external.DataContext === undefined ? oComponent_venPoRep.i18n('orderCount') : 'תונמזה תומכ'
                        }
                    },
                    categoryAxis: {
                        title: {
                            text: window.external.DataContext === undefined ? oComponent_venPoRep.i18n('VENDOR') : 'קפס'
                        }

                    }
                };
                var oProperties2 = {
                    plotArea: {
                        dataLabel: {
                            visible: true,
                            type: 'value',
                            showTotal: true,
                            hideWhenOverlap: false,
                        },
                        colorPalette: aPieColors

                    },
                    title: {
                        visible: false
                    },
                    legendGroup: {
                        layout: { position: 'bottom' }
                    }

                    // title: {
                    //     text: 'סטטוס הזמנות רכש',
                    //     alignment: "right"
                    // }
                };
                var oProperties3 = {
                    plotArea: {
                        formatString: formatPattern.SHORTFLOAT_MFD2,
                        dataLabel: {
                            visible: true,
                            showTotal: true,
                            hideWhenOverlap: false,
                        }

                    },
                    legend: {
                        visible: false
                    },
                    title: {
                        visible: false
                    },
                    valueAxis: {
                        label: {
                            formatString: formatPattern.SHORTFLOAT
                        },
                        title: {
                            text: window.external.DataContext === undefined ? oComponent_venPoRep.i18n('orderCount') : 'תונמזה תומכ'
                        }
                    },
                    categoryAxis: {
                        title: {
                            text: window.external.DataContext === undefined ? oComponent_venPoRep.i18n('po_group') : 'שכר תצובק'
                        }

                    }

                    // title: {
                    //     text: 'כמות/סכום הזמנות רכש לקבוצת רכש',
                    //     alignment: "right"
                    // }
                };

                // oPopOver.connect(_graph.getVizUid());
                // oPopOver.setFormatString(formatPattern.STANDARDFLOAT);
                _graph1.setVizProperties(oProperties1);
                _graph2.setVizProperties(oProperties2);
                _graph3.setVizProperties(oProperties3);
                // oPopOver.connect(_graph2.getVizUid());
                // oPopOver.setFormatString(ChartFormatter.DefaultPattern.STANDARDFLOAT);
            },
            onFilterGraph: function (oEvent, VizFrame, key, VizFrame2) {
                debugger;
                var oFilter = [];
                if (VizFrame === 'idVizFrame1' || VizFrame === 'idVizFrame1Dialog') {
                    var aQuery = oComponent_venPoRep.getModel("JSON").getProperty("/vendorGrahp") || [];
                    for (var i = 0; i < aQuery.length; i++) {
                        oFilter.push(new Filter('Vendor', FilterOperator.EQ, aQuery[i].Vendor));
                    }
                    var oSorter = new Sorter({ path: key, descending: true });
                    var growthRatesDataSet = new sap.viz.ui5.data.FlattenedDataset({
                        dimensions: [{
                            name: 'VendorName',
                            value: "{JSON>VendorName}"
                        }],
                        measures: [{
                            name: key,
                            value: "{JSON>" + key + "}"
                        }],
                        data: {
                            path: "JSON>/orderData/OrderRepVendorGrahp/results",
                            sorter: [oSorter],
                            filters: [oFilter]
                        }
                    });
                }
                else if (VizFrame === 'idVizFrame3' || VizFrame === 'idVizFrame3Dialog') {
                    var aQuery = oComponent_venPoRep.getModel("JSON").getProperty("/PurgroupGrahp") || [];
                    for (var i = 0; i < aQuery.length; i++) {
                        oFilter.push(new Filter('PurGroup', FilterOperator.EQ, aQuery[i].Purgroup));
                    }
                    var oSorter = new Sorter({ path: key, descending: true });
                    var growthRatesDataSet = new sap.viz.ui5.data.FlattenedDataset({
                        dimensions: [{
                            name: 'PurGroupName',
                            value: "{JSON>PurGroupName}"
                        }],
                        measures: [{
                            name: key,
                            value: "{JSON>" + key + "}"
                        }],
                        data: {
                            path: "JSON>/orderData/OrderRepPurGrpGrahp/results",
                            sorter: [oSorter],
                            filters: [oFilter]
                        }
                    });
                }

                //assign "new" dataset 
                var vizFrame = this.getView().byId(VizFrame) || sap.ui.core.Fragment.byId("vizFrameDialog", VizFrame);;
                vizFrame.setDataset(growthRatesDataSet);
            },
            onChangeDisplay: function (oEvent, VizFrame, dataPath, sKey) {
                debugger;
                var formatPattern = ChartFormatter.DefaultPattern;
                try { var key = oEvent.getParameter("item").getProperty("key"); } catch (e) {
                    var key = sKey;
                };
                var oSorter = new Sorter({ path: key, descending: true });
                var growthRatesDataSet = new sap.viz.ui5.data.FlattenedDataset({
                    dimensions: [{
                        name: VizFrame === 'idVizFrame1' || VizFrame === 'idVizFrame1Dialog' ? 'VendorName' : 'PurGroupName',
                        value: VizFrame === 'idVizFrame1' || VizFrame === 'idVizFrame1Dialog' ? '{JSON>VendorName}' : '{JSON>PurGroupName}'
                    }],
                    measures: [{
                        name: key,
                        value: "{JSON>" + key + "}"
                    }],
                    data: {
                        path: "JSON>/orderData/" + dataPath + "/results",
                        sorter: [oSorter]
                    }
                });

                //assign "new" dataset 
                var vizFrame = this.getView().byId(VizFrame) || sap.ui.core.Fragment.byId("vizFrameDialog", VizFrame);
                vizFrame.setDataset(growthRatesDataSet);
                vizFrame.removeAllFeeds();
                var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
                    'uid': "valueAxis",
                    'type': "Measure",
                    'values': [key]
                }),
                    feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
                        'uid': "categoryAxis",
                        'type': "Dimension",
                        'values': [VizFrame === 'idVizFrame1' || VizFrame === 'idVizFrame1Dialog' ? 'VendorName' : 'PurGroupName']
                    });
                vizFrame.addFeed(feedValueAxis);
                vizFrame.addFeed(feedCategoryAxis);
                var oProperties = {
                    plotArea: {
                        dataLabel: {
                            formatString: formatPattern.SHORTFLOAT_MFD2
                        }
                    },
                    valueAxis: {
                        label: {
                            formatString: formatPattern.SHORTFLOAT
                        },
                        title: {
                            text: key === 'Quantity' ? window.external.DataContext === undefined ? oComponent_venPoRep.i18n('orderCount') : 'תונמזה תומכ' : window.external.DataContext === undefined ? oComponent_venPoRep.i18n('orderSum') : 'תונמזה םוכס'
                        }
                    }
                };
                vizFrame.setVizProperties(oProperties);
                this.onFilterGraph('', VizFrame, key);

            },
            removeDuplicates: function (array, key) {
                let lookup = new Set();
                return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
            }
        });
    }
);
